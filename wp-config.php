<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_smeba');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         't_`fn6m-:;W+tn*4$LXRb#CQ=c^@f`>Y/MEu$Oo?q$>aR=@,0Q[-56!5R0LlJ$oV');
define('SECURE_AUTH_KEY',  'sDJOT4d|PSYPb-WyNbM_jh|LqL6s&61v}@<k5L]nflKneNOsFo3a|4sEsYw~~|#|');
define('LOGGED_IN_KEY',    '-gNRkb~jvm#H&IO_C!@mzRVpxnqH.Y{K.P9u~PYH]stoNd|Mj*[#&zS4Yq%Xx4L+');
define('NONCE_KEY',        '@ix!=BWe|I-d8_Lsoql.8 $-}got[:SFhp<xYKh6+RQVn~~oBO9X@?o+`nk7Tqk|');
define('AUTH_SALT',        'ddiJ0KUo<sR}bOCX~qXx+BF-8sC[ymKW6ys{|QTbJ~3qk)dx8,Ko^_/0RAOqh4 5');
define('SECURE_AUTH_SALT', 'o,$px>^=%H5hZTQJN6LW3zO+qbb<2p1.|-{;[G)~{T~5I+r6>w,uUK~@0Ecm77,n');
define('LOGGED_IN_SALT',   'l~sb0:_lM3o-{crJb%HqV|AM AM*?*wEj<|UVGU 4_XU&.7w^:LgH1V+%t|lg)Wx');
define('NONCE_SALT',       'R:RCaY^8d~x^Vm=9E4dG}L_IIg}kPawn*Gn~q[g,Ae%Bo6N2R$:)>O]=2Gi6V`#%');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
