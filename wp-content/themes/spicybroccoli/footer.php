<section id="contact-us" class="bg-blue-grey pg-home">

  <div class="container">

      <h2>Contact Us</h2>

      <p>Get in touch with us today!</p>

      <div class="row">

			<?php  echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>

      </div>

      </div>

  </div>

</section><!-- #contact-us -->

<section id="links-footer">

  <div class="container">

      <nav>

            <?php

	           	wp_nav_menu( 

	            	array( 

	            		'menu' => 'primary',

						'container' => ''

	            	) 

	            ); 

	        ?>

      </nav>

  </div>

</section><!-- #linksfooter -->

<section id="copyright">

  <div class="container">

     <?php echo date('Y'); ?> SME Business Accountants <span>|</span> <a href="#">Terms & Conditions</a><span>|</span><a href="#">Privacy Policy</a><span>|</span><a href="http://dev.spicybroccolitesting.com.au/smeba/newsletter/">Newsletter</a> <span>|</span>Graphic Design <a href="#">Sydney Spicy Broccoli Media </a>

  </div>

</section><!-- #copyright -->



<?php wp_footer(); ?>

<script>	new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );</script>

</body>

</html>