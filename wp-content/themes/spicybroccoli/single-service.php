<?php
/*
Template Name: Newsletter Archive
*/
?>
<?php get_header(); ?>
<section id="services" class="main-container">
	<div class="container">
    	<div class="col-left">
        <h3>Our Services</h3>
        <?php get_sidebar( 'service' ) ?>
        <aside id="news">
        <h4>Latest Newsletter</h4>
        <?php get_sidebar( 'newsletter' ) ?>
        </aside>
        </div>
        <div class="col-right">
        <article>
		<?php while( have_posts() ) : the_post() ?>
            <h1><?php the_title(); ?></h1>
                	
                <div class="service-container">
                  <p><?php the_content(); ?></p>
            	</div>
				<?php endwhile; ?>
                </article>
        </div>
    </div>
</section>
<?php get_footer(); ?>