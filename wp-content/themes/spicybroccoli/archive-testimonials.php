<?php get_header(); ?>

<section id="testimonials" class="main-container">
  <div class="container">
  <h1>
        Testimonials
      </h1>
      <?php while( have_posts() ) : the_post() ?>
      <div class="testimonial-container">
         <div class="row">
          <div class="col-size-1">
          	<div class="tetimonial-quotes <?php echo get_post_meta(get_the_ID(),'quotes_color',true); ?>">
       	    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial.png" class="img-circular img-responsive img-center" /> </div>
          </div>
          <div class="col-size-3">
		  	<?php the_content(); ?>
          </div>
          </div>
      </div>
      <?php endwhile; ?>
  </div>
</section>
<?php get_footer(); ?>
