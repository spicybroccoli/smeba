<?php get_header(); ?>

<section id="services" class="bg-blue-grey pg-home">
  <div class="container">
    <div class="col-left">
      <h2>Our Services</h2>
      <div class="service-staff"> <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/stuart.jpg" width="150" height="150"  alt="" class="img-circular"/>
        <p class="blue">Stuart Boyers, CEO</p>
      </div>
      <p>Click to the right to find out <br>
        more about our vast array <br>
        of experience in these service areas</p>
    </div>
    <div class="col-right">
      <div class="service-tiles">
        <div class="tiles">
          <?php  

                //WP QUERY ARGS

                $args = array(

					// GET THE LAST 8 ORDERED SERVICES

                    'post_type' => array( 'service' ),

                    'posts_per_page' => 8,

                    'order' => 'ASC',

                    'orderby' => 'menu_order',

                );

                

                $the_query = new WP_Query( $args );

		?>
          <?php while( $the_query->have_posts() ) : $the_query->the_post() ?>
          <a class="tile-img <?php echo getTitleClass(get_the_title(), 'serv-'); ?>"  href="<?php the_permalink(); ?>"><span>
          <?php the_title(); ?>
          </span></a>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- #services -->

<section id="news" class="pg-home">
  <div class="container">
    <h2>Latest News</h2>
    <?php  

                //WP QUERY ARGS

                $args = array(

					// GET THE LAST 8 ORDERED SERVICES

                    'post_type' => array( 'post' ),

                    'posts_per_page' => 3

                );

                

                $the_query = new WP_Query( $args );

				$count = 1;

		?>
    <div class="row-two-col">
      <?php while( $the_query->have_posts() ) : $the_query->the_post() ?>
      <?php ($count == 1 or $count == 2) ?>
      <div class="two-col">
        <div class="news-container">
          <h3>
            <?php the_title(); ?>
          </h3>
          <span> <?php echo get_the_date( 'F Y' ); ?> </span>
          <p>
            <?php 

				  ($count == 1)? $size = 630 : $size = 100;

				  echo getLimitedText(get_the_content(),$size); ?>
          </p>
          <a class="button-blue button-blue-green" href="<?php the_permalink(); ?>">Find Out More</a> </div>
        <?php ($count == 1 or $count == 3) ?>
      </div>
      <?php 

		$count ++;

		endwhile; ?>
    </div>
  </div>
  <div class="bg-section-bottom"></div>
</section>
<!-- #news -->

<section id="industry" class="bg-blue-grey pg-home">
  <div class="container">
    <div class="col-left">
      <h2>We are
        
        industry
        
        experts.</h2>
      <p>Do you belong to one of the industries on the right? if so than we can help.</p>
    </div>
    <div class="col-right">
      <div class="service-tiles">
        <div class="tiles"> <a href="#"><span>Consulting Services</span></a> <a href="#"><span>Advertising</span></a> <a href="#"><span>Plumbers & Painters</span></a> <a href="#"><span>Law Firms</span></a> <a href="#"><span>Financial Services</span></a> <a href="#"><span>Creative Agencies</span></a> <a href="#"><span>Retaillers</span></a> <a href="#"><span>Restaurants</span></a> </div>
      </div>
    </div>
  </div>
</section>
<!-- #industry -->

<section id="testimonial" class="pg-home">
  <div class="container">
    <h2>Our Happy Clients</h2>
    <div class="col-left">
    <div class="tetimonial-quotes ">
       	    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/testimonial.png" class="img-circular img-responsive img-center"> </div>
    </div>
    <div class="col-right">
      <div class="testimonial-txt">
        <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non  mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p>
        <span>Ed Ball, Managin Director<br>
        Web Scarlett DeVlam Australia Pty Ltd</span> </div>
    </div>
  </div>
  <div class="bg-section-bottom"></div>
</section>
<!-- #testimonial -->

<?php get_footer(); ?>
