    <div class="service-container">
    <?php  
            //WP QUERY ARGS
            $args = array(
                // GET THE LAST 8 ORDERED SERVICES
                'post_type' => array( 'service' ),
                'posts_per_page' => -1,
				'order' => 'ASC',
                'orderby' => 'menu_order'
            );
            
            $the_query = new WP_Query( $args );
    ?>
    <ul class="list-container">
    <?php 
	$current_id = get_the_ID();
	while( $the_query->have_posts() ) : $the_query->the_post() ?>
        <li class="<?php echo getTitleClass(get_the_title(), 'serv-'); ?><?php if($current_id == get_the_ID()){ echo " current";} ?>"><a href="<?php the_permalink(); ?>"><?php the_title();?> </a></li>
         <?php endwhile; ?>
         </ul>
    </div>