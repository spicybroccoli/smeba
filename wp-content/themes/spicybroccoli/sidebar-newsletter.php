    <div class="news-container">
    <?php  
            //WP QUERY ARGS
            $args = array(
                // GET THE LAST 8 ORDERED SERVICES
                'post_type' => array( 'post' ),
                'posts_per_page' => 4
            );
            
            $the_query = new WP_Query( $args );
    ?>
    <ul class="list-container">
    <?php while( $the_query->have_posts() ) : $the_query->the_post() ?>
        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?><span> <?php echo get_the_date( 'F Y' ); ?> </span></a></li>
         <?php endwhile; ?>
         </ul>
    </div>