<?php
/**
 * Theme Functions
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640;

/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', function() {

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
			'primary' => 'Primary Navigation',
		) );

});

// Bootstrap Theme
include('inc/template-utils.php'); // Load Template Utilities
include('inc/branding.php'); // Load Template Utilities
include('inc/responsive-nav.php'); // Responsive Nav
// include('inc/landing.php'); // Load Landing page if necessary
include('inc/scripts.php'); // Load Scripts
include('inc/cpt-service.php');
include('inc/cpt-testimonial.php');

// transform the title in a text able to use in CSS
function getTitleClass($title, $prefix = false) {

		$class_add = strtolower((preg_replace('/ /', '-', $title)));
		
		($prefix)?$class_add = $prefix.$class_add:'';
	
	return $class_add;
	
}
/* limit the content without split a word
* DO NOT USE the_content()
* USE get_the_content() INSTEAD
*/
function getLimitedText($content, $size = 200, $dodots = true, $strip = true) {
		
		($strip)?$content = wp_strip_all_tags($content):'';
		
		if(strlen($content) > $size){
			$pos = stripos( $content, ' ', $size);
			$content = substr( $content, 0, $pos ); 
			
			$dots = ($dodots)?'...':'';
		}
		
	return $content . $dots;
}
