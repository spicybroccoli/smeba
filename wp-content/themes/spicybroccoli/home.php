<?php
/*
Template Name: Newsletter Archive
*/
?>
<?php get_header(); ?>
<section id="news" class="main-container">
	<div class="container">
    	<div class="col-left">
        <h3>Archive</h3>
        <?php get_sidebar( 'newsletter' ) ?>
        </div>
        <div class="col-right">
            <h1>Latest Newsletter</h1>
                	<?php while( have_posts() ) : the_post() ?>
                <div class="news-container">
                  <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                  <span> <?php echo get_the_date( 'F Y' ); ?> </span>
                  <p><?php echo getLimitedText(get_the_content(),300); ?></p>
            	</div>
                <div class="news-container-bottom">
                bottom
                </div>
				<?php endwhile; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>