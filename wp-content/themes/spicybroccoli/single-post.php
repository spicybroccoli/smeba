<?php
/*
Template Name: Single Post
*/
?>
<?php get_header(); ?>

<section id="news" class="main-container">
  <div class="container">
    <div class="col-left">
      <h3>Archive</h3>
      <?php get_sidebar( 'newsletter' ) ?>
    </div>
    <div class="col-right">
    <?php while( have_posts() ) : the_post() ?>
    <article>
      <h1>
        <?php the_title(); ?> - <?php echo get_the_date( 'F Y' ); ?>
      </h1>
      <div class="news-container">
        <p><?php echo getLimitedText(get_the_content(),300); ?></p>
      </div>
      </div>
    </article>
    <?php endwhile; ?>
  </div>
</section>
<?php get_footer(); ?>
