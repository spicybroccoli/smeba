<?php
add_action( 'init', 'register_cpt_testimonials' );

function register_cpt_testimonials() {

    $labels = array( 
        'name' => _x( 'Testimonials', 'testimonials' ),
        'singular_name' => _x( 'Testimonial', 'testimonials' ),
        'add_new' => _x( 'Add New', 'testimonials' ),
        'add_new_item' => _x( 'Add New Testimonial', 'testimonials' ),
        'edit_item' => _x( 'Edit Testimonial', 'testimonials' ),
        'new_item' => _x( 'New Testimonial', 'testimonials' ),
        'view_item' => _x( 'View Testimonial', 'testimonials' ),
        'search_items' => _x( 'Search Testimonials', 'testimonials' ),
        'not_found' => _x( 'No testimonials found', 'testimonials' ),
        'not_found_in_trash' => _x( 'No testimonials found in Trash', 'testimonials' ),
        'parent_item_colon' => _x( 'Parent Testimonial:', 'testimonials' ),
        'menu_name' => _x( 'Testimonials', 'testimonials' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        'menu_icon' => 'dashicons-testimonial',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'testimonials', $args );
}