<?php get_header(); ?>
<section id="pages" class="main-container">
	<div class="container">
    	<div class="col-left">
        <aside id="services">
            <h3>Our Services</h3>
            <?php get_sidebar( 'service' ) ?>
        </aside>
        </div>
        <section class="col-right">
		
            <h1>Search results for: <?php echo get_search_query(); ?></h1>
            <div class="news-container">
			<?php if(have_posts()): ?>
				<ul id="search-list">
				<?php while(have_posts()): the_post(); ?>
                <li>
                    <h2> <a href="<?php the_permalink(); ?> "><?php the_title(); ?></a></h2>
          			<p><?php echo getLimitedText(get_the_content()); ?>
                    <a href="<?php the_permalink(); ?> ">Read More</a>  </p>
                    
                    </li>
				<?php endwhile; ?>
                <ul>
                <?php else: ?>
                    <p align="center">Sorry we couldn't find anything that matched <strong><?php echo get_search_query(); ?></strong></p>
                <?php endif; ?>
               </div>
        </section>
    </div>
</section>
<?php get_footer(); ?>