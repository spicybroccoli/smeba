<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title>
<?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?>
</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="mp-pusher" class="mp-pusher">
<nav class="mp-menu" id="mp-menu">
  <div class="mp-level">
    <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
	            		'container'  => 'false',
	            		'container_id' => 'mp-menu',
	            		'menu_class' => 'mp-level',
	            		'container_class' => 'mp-menu',
	            		'menu_class' => '',  
	            		'walker' => new SBM_Responsive_Nav()
	            	) 
	            ); 
	        ?>
  </div>
</nav>
<div id="top-menu">  </div>
<div id="wrapper" class="hfeed">
<section id="header" class="bg-blue-grey">
<a href="#" id="trigger" class="menu-trigger"></a>
  <div class="container">
    <div class="col-left">
      <div class="logo"> <a href="http://dev.spicybroccolitesting.com.au/smeba"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="<?php bloginfo( 'name' ); ?>"/></a> </div>
    </div>
    <div class="col-right">
      <div class="row">
        <div class="col-size-2 header-search">
          <div class="search">
            <form role="search" method="get" id="searchform" class="searchform" action="<?php echo home_url( '/' ); ?>" >
              <div>
                <input type="text" value="" name="s" id="s">
                <input type="submit" id="searchsubmit" value="Search">
              </div>
            </form>
          </div>
        </div>
        <div class="col-size-2 header-contact">
          <div class="phone"><span>Call Today</span> 02 9411 2644</div>
        </div>
        <div class="col-size-4 main-menu">
            <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'primary',
						'container' => ''
	            	) 
	            ); 
	        ?>
        </div>
      </div>
    </div>
  </div>
</section><!-- #header -->

<?php 
	if(is_front_page()):
?>
<section id="slider" class="bg-grey">
<?php 
    echo do_shortcode("[metaslider id=26]");
?>
</section><!-- #slider -->
<?php
	else:
?>
<section id="slider-internal" class="bg-grey"></section>
<?php endif; ?>
